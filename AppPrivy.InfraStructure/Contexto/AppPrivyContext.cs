﻿using AppPrivy.CrossCutting.Commom;
using AppPrivy.Domain;
using AppPrivy.Domain.Entities.Blog;
using AppPrivy.Domain.Entities.DoacaoMais;
using AppPrivy.InfraStructure.EntityConfig.Blog;
using AppPrivy.InfraStructure.EntityConfig.DoacaoMais;
using AppPrivy.InfraStructure.EntityConfig.Identity;
using AppPrivy.InfraStructure.EntityConfig.Site;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;

namespace AppPrivy.InfraStructure.Contexto
{

    public class AppPrivyContext : IdentityDbContext
    {
        private readonly IConfiguration _configuration;

        public AppPrivyContext(DbContextOptions<AppPrivyContext> options, IConfiguration configuration) : base(options)
        {
            _configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_configuration.GetConnectionString(ConstantHelper.AppPrivyContext), builder => builder.EnableRetryOnFailure());
            }
        }

        //-------------------------------Doacao Mais-----------------------

        public virtual DbSet<Estatistico> Estatistico { get; set; }
        public virtual DbSet<Bazar> Bazar { get; set; }
        public virtual DbSet<Caccc> Caccc { get; set; }
        public virtual DbSet<Campanha> Campanha { get; set; }
        public virtual DbSet<ContaBancaria> ContaBancaria { get; set; }
        public virtual DbSet<Conteudo> Conteudo { get; set; }
        public virtual DbSet<Noticia> Evento { get; set; }     
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<Notificacao> Notificacao { get; set; }
        public virtual DbSet<Dispositivo> Dispositivo { get; set; }

        public virtual DbSet<NotificacaoDispositivo> NotificacaoDispositivo { get; set; }

        //-------------------------------Site-----------------------
        public virtual DbSet<Pesquisa> Pesquisa { get; set; }

        //-------------------------------Blog-----------------------
        public virtual DbSet<Author> Author { get; set; }
        public virtual DbSet<Category> Categorie { get; set; }
        public virtual DbSet<Post> Post { get; set; }
        public virtual DbSet<PostCategory> PostCategories { get; set; }
        public virtual DbSet<PostComment> PostComments { get; set; }
        public virtual DbSet<PostMeta> PostMeta { get; set; }
        public virtual DbSet<PostTag> PostTags { get; set; }
        public virtual DbSet<Tag> Tag { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            //---------------------------Identity-----------------------

            modelBuilder.ApplyConfiguration(new IdentityRoleClaimConfiguration());
            modelBuilder.ApplyConfiguration(new IdentityRoleConfiguration());
            modelBuilder.ApplyConfiguration(new IdentityUserClaimConfiguration());
            modelBuilder.ApplyConfiguration(new IdentityUserConfiguration());
            modelBuilder.ApplyConfiguration(new IdentityUserLoginConfiguration());
            modelBuilder.ApplyConfiguration(new IdentityUserRoleConfiguration());
            modelBuilder.ApplyConfiguration(new IdentityUserTokenConfiguration());

            //-------------------------------Doacao Mais-----------------------
            modelBuilder.ApplyConfiguration(new EstatisticoConfiguration());
            modelBuilder.ApplyConfiguration(new BazarConfiguration());
            modelBuilder.ApplyConfiguration(new CacccConfiguration());
            modelBuilder.ApplyConfiguration(new CampanhaConfiguration());
            modelBuilder.ApplyConfiguration(new ContaBancariaConfiguration());
            modelBuilder.ApplyConfiguration(new ConteudoConfiguration());
            modelBuilder.ApplyConfiguration(new DispositivoConfiguration());           
            modelBuilder.ApplyConfiguration(new NoticiaConfiguration());
            modelBuilder.ApplyConfiguration(new NotificacaoConfiguration());
            modelBuilder.ApplyConfiguration(new UsuarioConfiguration());
            modelBuilder.ApplyConfiguration(new NotificacaoDispositivoConfiguration());

            //-------------------------------Site-----------------------
            modelBuilder.ApplyConfiguration(new PesquisaConfiguration());

            //-------------------------------Blog-----------------------
            modelBuilder.ApplyConfiguration(new AuthorConfiguration());
            modelBuilder.ApplyConfiguration(new CategoryConfiguration());
            modelBuilder.ApplyConfiguration(new TagConfiguration());
            modelBuilder.ApplyConfiguration(new PostConfiguration());
            modelBuilder.ApplyConfiguration(new PostCategoryConfiguration());
            modelBuilder.ApplyConfiguration(new PostCommentConfiguration());
            modelBuilder.ApplyConfiguration(new PostMetaConfiguration());
            modelBuilder.ApplyConfiguration(new PostTagConfiguration());



            modelBuilder.HasDefaultSchema("dbo").HasAnnotation("Relational:Collation", "Latin1_General_CI_AS"); ;
        }


        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("DataCadastro") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("DataCadastro").CurrentValue = DateTime.Now;
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property("DataCadastro").IsModified = true;
                }
            }
            return base.SaveChanges();
        }
    }
}
//Generate Migration

//1) dotnet ef --startup-project  ../AppPrivy/AppPrivy.WebAppMvc/AppPrivy.WebAppMvc.csproj migrations add IniciandoMigrations --context AppPrivyContext

//Generate DataBase

//2) dotnet ef --startup-project  ../AppPrivy/AppPrivy.WebAppMvc/AppPrivy.WebAppMvc.csproj database update -c AppPrivyContext --Verbose
