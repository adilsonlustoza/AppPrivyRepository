﻿using AppPrivy.Domain.Interfaces.Repositories.DoacaoMais;
using AppPrivy.InfraStructure.Contexto;
using AppPrivy.InfraStructure.Interface;
using AppPrivy.InfraStructure.Repositories.Blog;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AppPrivy.Test
{

    [TestClass]
    public class PostOneTest
    {
        private const string AppPrivyContextKey = "ContextManager.AppPrivy";

        private  IContextManager contextManager;
        private  IPostRepository postRepository;

        private readonly Mock<IHttpContextAccessor> mockHttpContextAcessor;
        private readonly Mock<IConfigurationSection> mockConfSection;
        private readonly IConfiguration configuration;
        private readonly DefaultHttpContext defaultHttpContext;
       
        public PostOneTest()
        {
            mockHttpContextAcessor = new Mock<IHttpContextAccessor>();
            mockHttpContextAcessor.Setup(o => o.HttpContext.User.Identity.Name).Returns("Adilson");

            defaultHttpContext = new DefaultHttpContext();
            defaultHttpContext.Items.Add(AppPrivyContextKey, null);


            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory() )
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)               
                .AddEnvironmentVariables();

            configuration = builder.Build();



          

            contextManager = new ContextManager(mockHttpContextAcessor.Object, configuration);
        }


        [TestMethod]
        public async Task PostRepositoryTestAsync()
        {

            try
            {
                postRepository = new PostRepository(contextManager);
                var list = await postRepository.ListAllPosts();
                Assert.IsTrue(list.Any(), "Tested Passed");
            }
            catch (System.Exception ex)
            {

                throw ex;
            }     
         

        }
    }
}
