﻿using AppPrivy.Application.Interfaces;
using AppPrivy.Application.ViewsModels.Blog;
using AppPrivy.Domain.Entities.Blog;
using AppPrivy.Domain.Interfaces;
using AppPrivy.Domain.Interfaces.Services.DoacaoMais;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AppPrivy.Application.Services.Blog
{
    public class PostAppService : IPostAppService
    {
        private readonly IPostService _postService;
        private IMapper _mapper;

        public PostAppService(IPostService postService, IMapper mapper)
        {
            _postService = postService;
            _mapper = mapper;
        }

        public async void Add(PostViewModel obj)
        {
            try
            {
                await _postService.Add(_mapper.Map<PostViewModel, Post>(obj));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async void Update(PostViewModel obj)
        {
            try
            {
                await _postService.Update(_mapper.Map<PostViewModel, Post>(obj));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async void Remove(PostViewModel obj)
        {
            try
            {
                await _postService.Remove(_mapper.Map<PostViewModel, Post>(obj));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
      
        public async Task<ICollection<PostViewModel>> GetAll(params Expression<Func<PostViewModel, object>>[] children)
        {
            try
            {
                var posts = await _postService.GetAll();
                return _mapper.Map<ICollection<Post>, ICollection<PostViewModel>>(posts);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<PostViewModel> GetById(int id)
        {
            try
            {
                var post = await _postService.GetById(id);
                return _mapper.Map<Post, PostViewModel>(post);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Task<ICollection<PostViewModel>> Search(Expression<Func<PostViewModel, bool>> expression, params Expression<Func<PostViewModel, object>>[] children)
        {
            throw new NotImplementedException();
        }
        
        public void Dispose()
        {
            _postService.Dispose();

        }

        public async Task<int> SavePostAsync(PostViewModel post)
        {
            try
            {
                return await _postService.SavePostAsync(_mapper.Map<PostViewModel, Post>(post));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task UpdatePostAsync(int? Id, PostViewModel post)
        {
            try
            {
                await _postService.UpdatePostAsync(Id,_mapper.Map<PostViewModel, Post>(post));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public  async Task<PostViewModel> GetPostsById(int? Id)
        {
            try
            {
                var post = await _postService.GetPostsById(Id);
                return  _mapper.Map<Post, PostViewModel>(post);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<PostViewModel> GetPostsByGuid(Guid guid)
        {
            try
            {
                var post = await _postService.GetPostsByGuid(guid);
                return _mapper.Map<Post, PostViewModel>(post);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<PostViewModel>> ListAllPosts()
        {
            try
            {
                var posts = await _postService.ListAllPosts();
                return _mapper.Map<IEnumerable<Post>, IEnumerable<PostViewModel>>(posts);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<PostViewModel>> ListAllPostsByCriteria(Expression<Func<PostViewModel, bool>> expression)
        {
            try
            {
                var postExpression = _mapper.Map<Expression<Func<Post, bool>>>(expression);            
                var posts = await _postService.ListAllPostsByCriteria(postExpression);
                return _mapper.Map<IEnumerable<Post>, IEnumerable<PostViewModel>>(posts);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
