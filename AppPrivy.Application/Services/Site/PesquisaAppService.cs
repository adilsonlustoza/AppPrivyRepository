﻿using AppPrivy.Application.Interfaces;
using AppPrivy.Application.ViewsModels;
using AppPrivy.Domain;
using AppPrivy.Domain.Interfaces;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AppPrivy.Application
{
    public class PesquisaAppService : IPesquisaAppService
    {
        private readonly IPesquisaService _pesquisaService;
        private IMapper _mapper;
       
        public PesquisaAppService(IPesquisaService pesquisaService,IMapper mapper)
        {
            _pesquisaService = pesquisaService;
            _mapper = mapper;
        }

        public void Add(PesquisaViewModel obj)
        {
             _pesquisaService.Add(_mapper.Map<PesquisaViewModel, Pesquisa>(obj));
        }

        public void Remove(PesquisaViewModel obj)
        {
            _pesquisaService.Remove(_mapper.Map<PesquisaViewModel, Pesquisa>(obj));
        }


        public void Update(PesquisaViewModel obj)
        {
           _pesquisaService.Update(_mapper.Map<PesquisaViewModel, Pesquisa>(obj));
        }


        #region [-------------------------------------Query----------------------------------]
        public async Task<PesquisaViewModel> GetById(int id)
        {
            return _mapper.Map<Pesquisa, PesquisaViewModel>(await _pesquisaService.GetById(id));
        }

        public async Task<ICollection<PesquisaViewModel>> GetAll(params Expression<Func<PesquisaViewModel, object>>[] children)
        {
          
            var result = await _pesquisaService.GetAll();
            return _mapper.Map<ICollection<Pesquisa>, ICollection<PesquisaViewModel>>(result);
        }

        public async Task<ICollection<PesquisaViewModel>> Search(string descricao, params Expression<Func<PesquisaViewModel, object>>[] children)
        {
            try
            {

                var lstPesquisa = await _pesquisaService.Search(p => p.Descricao.Contains(descricao));

                if (lstPesquisa != null)
                    return _mapper.Map<List<Pesquisa>, List<PesquisaViewModel>>(lstPesquisa.ToList());                

                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task<ICollection<PesquisaViewModel>> Search(Expression<Func<PesquisaViewModel, bool>> expression, params Expression<Func<PesquisaViewModel, object>>[] children)
        {
            throw new NotImplementedException();
        }

        #endregion

        public void Dispose()
        {
            _pesquisaService.Dispose();
        }
    }

}
