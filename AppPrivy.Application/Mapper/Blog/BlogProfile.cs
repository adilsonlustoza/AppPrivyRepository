﻿using AppPrivy.Application.ViewsModels.Blog;
using AppPrivy.Domain.Entities.Blog;
using AutoMapper;

namespace AppPrivy.Application.Mapper.Blog
{
    public class BlogProfile:Profile
    {
        public BlogProfile()
        {
            CreateMap<Post, PostViewModel>()                
              .ReverseMap();
        }
    }
}
