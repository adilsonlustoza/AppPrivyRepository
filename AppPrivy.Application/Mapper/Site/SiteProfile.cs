﻿using AppPrivy.Application.ViewsModels;
using AppPrivy.Domain;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppPrivy.Application.Mapper.Site
{
    public class SiteProfile:Profile
    {

        public SiteProfile()
        {
            CreateMap<Pesquisa, PesquisaViewModel>()
              .ReverseMap();
             
       
        }
    }
}
