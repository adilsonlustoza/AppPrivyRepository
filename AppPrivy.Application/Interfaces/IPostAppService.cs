﻿using AppPrivy.Application.ViewsModels.Blog;
using AppPrivy.Domain.Entities.Blog;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AppPrivy.Application.Interfaces
{
    public interface IPostAppService : IAppServiceBase<PostViewModel>
    {
        public Task<int> SavePostAsync(PostViewModel post);

        public Task UpdatePostAsync(int? Id, PostViewModel post);

        public Task<PostViewModel> GetPostsById(int? Id);

        public Task<PostViewModel> GetPostsByGuid(Guid guid);

        public Task<IEnumerable<PostViewModel>> ListAllPosts();

        public Task<IEnumerable<PostViewModel>> ListAllPostsByCriteria(Expression<Func<PostViewModel, bool>> expression);
    }
}
