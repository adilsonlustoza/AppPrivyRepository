﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppPrivy.Application.ViewsModels
{
    public  abstract class SuperViewModel
    {
        public DateTime? DtPrivyRegister { get; set; }
        public Guid UniquePrivyId { get; set; }
    }
}
