﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppPrivy.Application.ViewsModels.Blog
{
    
    public partial class PostTagViewModel : SuperViewModel
    {
      
        public long PostId { get; set; }

      
        public long TagId { get; set; }

    
        public virtual PostViewModel Post { get; set; }
     
        public virtual TagViewModel Tag { get; set; }
    }
}
