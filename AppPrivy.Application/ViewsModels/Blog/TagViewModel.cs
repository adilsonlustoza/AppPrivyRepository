﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppPrivy.Application.ViewsModels.Blog
{
    
    public partial class TagViewModel : SuperViewModel
    {
        public TagViewModel()
        {
            PostTags = new HashSet<PostTagViewModel>();
        }

        [Key]
        public long TagId { get; set; }

        [Required]
        [StringLength(75)]
        public string Title { get; set; }
        
        [StringLength(100)]
        public string MetaTitle { get; set; }
        
        [Required]
        [StringLength(100)]
        public string Slug { get; set; }
        
        public string Content { get; set; }


        public virtual ICollection<PostTagViewModel> PostTags { get; set; }
    }
}
