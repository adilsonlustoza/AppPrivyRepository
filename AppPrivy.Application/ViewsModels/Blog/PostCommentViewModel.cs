﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppPrivy.Application.ViewsModels.Blog
{
   
    public partial class PostCommentViewModel : SuperViewModel
    {
        [Key]
        public long PostCommentId { get; set; }
        public long PostId { get; set; }
        public long? ParentId { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }
        public short Published { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? PublishedAt { get; set; }
        public string Content { get; set; }     
        public virtual PostViewModel Post { get; set; }
    }
}
