﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppPrivy.Application.ViewsModels.Blog
{  
    public partial class PostViewModel: SuperViewModel
    {
        public PostViewModel()
        {
            InverseParent = new HashSet<PostViewModel>();
            Category = new HashSet<CategoryViewModel>();
            PostComments = new HashSet<PostCommentViewModel>();
            PostTags = new HashSet<PostTagViewModel>();
        }

        [Key]
        public long PostId { get; set; }
        public long AuthorId { get; set; }
        public long? ParentId { get; set; }

        public long? CategoryId { get; set; }

        [Required(ErrorMessage ="{0} é requerido.")]
        [StringLength(75)]
        [DisplayName("Título")]
        public string Title { get; set; }

        [StringLength(100)]
        [DisplayName("Sub Título")]
        public string MetaTitle { get; set; }

        [Required(ErrorMessage = "{0} é requerido.")]
        [DisplayName("Resumo")]
        [StringLength(100)]
        public string Slug { get; set; }
        [StringLength(255)]


        [Required(ErrorMessage = "{0} é requerido.")]
        [DisplayName("Sumário")]
        public string Summary { get; set; }
        public short Published { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? PublishedAt { get; set; }

        [Required(ErrorMessage = "{0} é requerido.")]
        [DisplayName("Conteúdo")]
        public string Content { get; set; }


        [Required(ErrorMessage = "{0} é requerida.")]
        [DisplayName("Url da imagem")]
        [StringLength(255)]
        public string UrlImage { get; set; }
             
        public virtual AuthorViewModel Author { get; set; }     
        public virtual PostViewModel Parent { get; set; }     
        public virtual PostMetaViewModel PostMeta { get; set; }

        public SelectList CategorySelectList { get; set; }

        public SelectList AuthorSelectList { get; set; }


        public virtual ICollection<PostViewModel> InverseParent { get; set; }
        public virtual ICollection<CategoryViewModel> Category { get; set; }      
        public virtual ICollection<PostCommentViewModel> PostComments { get; set; }     
        public virtual ICollection<PostTagViewModel> PostTags { get; set; }
    }
}
