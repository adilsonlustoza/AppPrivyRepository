﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppPrivy.Application.ViewsModels.Blog
{
  
    public partial class CategoryViewModel : SuperViewModel
    {
        public CategoryViewModel()
        {
            InverseParent = new HashSet<CategoryViewModel>();
            PostCategories = new HashSet<PostCategoryViewModel>();
        }

        [Key]
        public long CategoryId { get; set; }
        public long? ParentId { get; set; }

        [Required]
        [StringLength(75)]
        public string Title { get; set; }
        [StringLength(100)]
        public string MetaTitle { get; set; }
        [Required]
        [StringLength(100)]
        public string Slug { get; set; }
        public string Content { get; set; }
        [StringLength(255)]
        public string UrlImage { get; set; }     
        public virtual CategoryViewModel Parent { get; set; }          
        public virtual ICollection<CategoryViewModel> InverseParent { get; set; }
        public virtual ICollection<PostCategoryViewModel> PostCategories { get; set; }
    }
}
