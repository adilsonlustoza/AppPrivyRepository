﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppPrivy.Application.ViewsModels.Blog
{
    
    public partial class PostMetaViewModel : SuperViewModel
    {
        [Key]
        public long PostMetaId { get; set; }

        public long PostId { get; set; }
        
        [Required]
        [StringLength(50)]
        public string Nkey { get; set; }
        
        [StringLength(500)]
        public string Content { get; set; }

       public virtual PostViewModel Post { get; set; }
    }
}
