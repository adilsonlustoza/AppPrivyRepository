﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace  AppPrivy.Application.ViewsModels.Blog
{
 
    public partial class PostCategoryViewModel : SuperViewModel
    {
        [Key]
        public long PostId { get; set; }
        [Key]
        public long CategoryId { get; set; }

        public virtual CategoryViewModel Category { get; set; }
    
        public virtual PostViewModel Post { get; set; }
    }
}
