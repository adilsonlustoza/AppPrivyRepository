﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppPrivy.Application.ViewsModels.Blog
{
   
    public partial class ExampleViewModel
    {
       

        [Key]
        public long AuthorId { get; set; }

        [StringLength(150)]
        public string Name { get; set; }
        [StringLength(15)]
     
        public DateTime RegisteredAt { get; set; }
       
        public string UrlImage { get; set; }

        public bool IsValid { get; set; }


        public byte[] Image { get; set; }


    }
}
