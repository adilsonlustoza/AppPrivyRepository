﻿
using AppPrivy.Domain.Entities.Blog;

namespace AppPrivy.Domain.Interfaces.Repositories.DoacaoMais
{
    public interface ITagRepository : IRepositoryBase<Tag>
    {
        
    }
}
