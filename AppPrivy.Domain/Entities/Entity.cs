using System;

namespace AppPrivy.Domain.Entities
{
    public abstract class Entity
    {
        private Guid _uniquePrivyId;
        private DateTime? _dtPrivyRegister;

        public Entity()
        {
            this._uniquePrivyId = Guid.NewGuid();
            this._dtPrivyRegister = DateTime.Now;
        }

        public virtual DateTime? DtPrivyRegister { get => _dtPrivyRegister; set => _dtPrivyRegister = value; }
        public virtual Guid UniquePrivyId { get => _uniquePrivyId; set => _uniquePrivyId = value; }
    }
}