﻿namespace AppPrivy.CrossCutting.Commom
{
    public static class ConstantHelper
    {
     
        public static readonly string GrupoAdministrador = Resources.Constants.GrupoAdministrador;
        public static readonly string GrupoBlog = Resources.Constants.GrupoBlog;
        public static readonly string GrupoSistemas = Resources.Constants.GrupoSistemas;
        public static readonly string AppPrivy_WebAppMvc = Resources.Constants.AppPrivy_WebAppMvc;
        public static readonly string AuthenticationCookieName = Resources.Constants.AuthenticationCookieName;
        public static readonly string AppPrivyContext = Resources.Constants.AppPrivyContext;
        public static readonly string AppPrivyLogFolderPath = Resources.Constants.AppPrivyLogFolderPath;
    }
}
