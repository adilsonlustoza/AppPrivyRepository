﻿namespace AppPrivy.CrossCutting.Agregation
{
    public class ContactAgregation
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Subject { get; set; }
        public byte[] File { get; set; }
        public string Message { get; set; }
        public int ContactType { get; set; }
        public bool Notification { get; set; }
        public string Body { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Path { get; set; }
        public string Url { get; set; }

    }
}
